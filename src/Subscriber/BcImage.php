<?php declare(strict_types=1);

namespace Blur\CoreSuite\Subscriber;

use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class BcImage implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        // Return the events to listen to as array like this:  <event to listen to> => <method to execute>
        return [
            KernelEvents::REQUEST => 'onStorefrontLoaded'
        ];
    }

    public function onStorefrontLoaded( RequestEvent $event )
    {
        $request = $event->getRequest();
        $template = $request->attributes->get('_template');
        #if (!$template instanceof Template) {
        #    return;
        #}

        // Do something


        #dd($request);
    }
}