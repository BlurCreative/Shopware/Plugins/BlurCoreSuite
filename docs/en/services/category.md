# Category Services

A collection of several category services that act like helpers.  

**Service ID**  
`blur_core_suite.services.category`  

**Service Class**
`Blur\CoreSuite\Services\Category`

## Service Methods 

### `getCategory( string $categoryId, SalesChannelContext $context, ?bool $fetchMedia = null ): CategoryEntity`

#### Description

Get the category object as `CategoryEntity` by the provided ID.  
Optional you can fetch the associated Media Entity.

#### Response

- `CategoryEntity`  
    `use Shopware\Core\System\SalesChannel\SalesChannelContext;`

#### Arguments

- `( string ) $categoryId`  
    *(Required)* The ID of the category you want to fetch

- `( SalesChannelContext ) $context`  
    *(Required)* The context of SalesChannel

- `( bool | null ) $fetchMedia`  
    *(Optional)* Determines whether the associated media entity should be fetched or not
    *Default value: `null`*